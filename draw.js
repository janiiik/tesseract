var svgns = "http://www.w3.org/2000/svg";

function generateCircularPoints(n, center, radius) {
  var points = [];
  for (var i = 0; i < n; i++) {
    var increment = ((2 * Math.PI) / n) * i;
    points.push({
      x: radius * Math.cos(increment) + center.x,
      y: radius * Math.sin(increment) + center.y
    });
  }
  return points;
}

function drawPointsToSvg(points, svg, pointRadius) {
  points.forEach(point => {
    var circle = document.createElementNS(svgns, "circle");
    circle.setAttributeNS(null, "cx", point.x);
    circle.setAttributeNS(null, "cy", point.y);
    circle.setAttributeNS(null, "r", pointRadius || 3);

    svg.appendChild(circle);
  });
}

function connectEachPointWithLine(
  points,
  svg,
  lineWidths,
  colorRgb = "rgb(0,0,0)"
) {
  for (var i = 0; i < points.length; i++) {
    for (var j = i + 1; j < points.length; j++) {
      var point1 = points[i];
      var point2 = points[j];

      var line = document.createElementNS(svgns, "line");
      line.setAttributeNS(null, "x1", point1.x);
      line.setAttributeNS(null, "x2", point2.x);
      line.setAttributeNS(null, "y1", point1.y);
      line.setAttributeNS(null, "y2", point2.y);
      line.setAttributeNS(
        null,
        "style",
        `stroke:${colorRgb};stroke-width: ` + lineWidths
      );

      svg.appendChild(line);
    }
  }
}

function connectEachPointWithExtendedLine(points, svg, lineLength, lineWidths) {
  for (var i = 0; i < points.length; i++) {
    for (var j = i + 1; j < points.length; j++) {
      var point1 = points[i];
      var point2 = points[j];

      var middle = {
        x: (point2.x + point1.x) / 2,
        y: (point2.y + point1.y) / 2
      };

      var vector2 = new Vector(point2.x - middle.x, point2.y - middle.y);
      vector2.mag = lineLength;

      var point2 = vector2.add(middle);
      var point1 = vector2.mulScalar(-1).add(middle);

      var line = document.createElementNS(svgns, "line");
      line.setAttributeNS(null, "x1", point1.x);
      line.setAttributeNS(null, "x2", point2.x);
      line.setAttributeNS(null, "y1", point1.y);
      line.setAttributeNS(null, "y2", point2.y);
      line.setAttributeNS(
        null,
        "style",
        "stroke:rgb(0,0,0);stroke-width: " + lineWidths
      );

      svg.appendChild(line);
    }
  }
}

function drawCircles(points, svg, radius, lineWidths) {
  points.forEach(point => {
    var circle = document.createElementNS(svgns, "circle");
    circle.setAttributeNS(null, "cx", point.x);
    circle.setAttributeNS(null, "cy", point.y);
    circle.setAttributeNS(null, "r", radius);
    circle.setAttribute("fill", "rgba(0,0,0,0)");
    circle.setAttribute("stroke", "black");
    circle.setAttribute("stroke-width", lineWidths);
    svg.appendChild(circle);
  });
}

function generatePhiPoints(
  center,
  pointsDistanceChange,
  radius,
  minDistanceToCenter
) {
  var phi = 1.618033988749895;
  var currentDistance = minDistanceToCenter || 0;
  var currentAngle = 0;
  let result = [];
  while (currentDistance < radius) {
    result.push({
      x: Math.cos(currentAngle) * currentDistance + center.x,
      y: Math.sin(currentAngle) * currentDistance + center.y
    });

    currentAngle = currentAngle + (2 * Math.PI) / phi;
    currentDistance = currentDistance + pointsDistanceChange;
  }
  return result;
}

function conndectPointsWithCenter(points, center, svg, lineWidths) {
  points.forEach(point => {
    var line = document.createElementNS(svgns, "line");
    line.setAttributeNS(null, "x1", point.x);
    line.setAttributeNS(null, "x2", center.x);
    line.setAttributeNS(null, "y1", point.y);
    line.setAttributeNS(null, "y2", center.y);
    line.setAttributeNS(
      null,
      "style",
      "stroke:rgb(0,0,0);stroke-width: " + lineWidths
    );

    svg.appendChild(line);
  });
}

function drawCirclesFromPointsToCenter(points, center, svg, lineWidths) {
  points.forEach(point => {
    var circle = document.createElementNS(svgns, "circle");
    circle.setAttributeNS(null, "cx", (point.x + center.x) / 2);
    circle.setAttributeNS(null, "cy", (point.y + center.y) / 2);
    circle.setAttributeNS(
      null,
      "r",
      Math.sqrt((point.x - center.x) ** 2 + (point.y - center.y) ** 2) / 2
    );
    circle.setAttribute("fill", "rgba(255,255,255,0.5)");
    circle.setAttribute("stroke", "black");
    circle.setAttribute("stroke-width", lineWidths);
    svg.appendChild(circle);
  });
}

function drawPolygonFromPointsToCenter(
  numberOfAngles,
  points,
  center,
  svg,
  lineWidths
) {
  points.forEach(point => {
    const polygonCenter = {
      x: (point.x - center.x) / 2 + center.x,
      y: (point.y - center.y) / 2 + center.x
    };
    const polygonStartToCenterVector = new Vector(
      center.x - polygonCenter.x,
      center.y - polygonCenter.y
    );

    const polygonPoints = [];
    for (var i = 0; i < numberOfAngles; i++) {
      const vector = polygonStartToCenterVector.clone();
      vector.angleDeg += (i / numberOfAngles) * 360;
      polygonPoints.push(vector.sub(polygonStartToCenterVector).add(center));
    }

    var polygon = document.createElementNS(svgns, "polygon");

    polygon.setAttributeNS(
      null,
      "points",
      polygonPoints.map(p => p.x + "," + p.y).join(" ")
    );
    polygon.setAttribute(
      "fill",
      `rgba(255, ${parseInt(
        ((points.length - points.indexOf(point)) / points.length) * 255
      )}, 255, 1)`
    );
    polygon.setAttribute("stroke", "black");
    polygon.setAttribute("stroke-width", lineWidths);
    svg.appendChild(polygon);
  });
}

function getParams() {
  var params = {};
  params.numberOfPoints = document.getElementById("sizeInput").value;
  params.radius = document.getElementById("radiusInput").value;
  params.center = { x: 400, y: 400 };
  params.strokeSize = document.getElementById("strokeSizeInput").value;
  params.sequnceNumber = document.getElementById("sequenceInput").value;
  params.svg = document.getElementById("svgId");

  return params;
}

function angleToRgb(angleDeg) {
  let red = 0;
  if (angleDeg > 120) red = parseInt(((360 - angleDeg) / 120) * 255);
  let blue = 0;
  if (angleDeg < 240) blue = parseInt((angleDeg / 240) * 255);
  let green = 0;
  if (angleDeg < 120) green = parseInt(((120 - angleDeg) / 120) * 255);
  if (angleDeg > 240) green = parseInt(((360 - angleDeg) / 120) * 255);

  const angleDiff = 360 - angleDeg;

  return `rgb(${red},${green}, ${blue})`;
}

function connectSpiralsWithColor(numberOfSpirals, phiDots, svg, strokeSize) {
  for (let i = 0; i < numberOfSpirals; i++) {
    const phiAngleRatio = ((i * 1.618033988749895 * 360) % 360) / 360;
    const red = parseInt(255 * phiAngleRatio * ((i % 3) / 2));
    const green = parseInt(255 * phiAngleRatio * (((i + 1) % 3) / 2));
    const blue = parseInt(255 * phiAngleRatio * (((i + 2) % 3) / 2));
    const color = `rgb(${red},${green},${blue})`;
    connectEachPointWithLine(
      phiDots.filter((_, idx) => (idx + i) % numberOfSpirals === 0),
      svg,
      strokeSize,
      color
    );
    console.log(color);
  }
}

function redrawSvg(params) {
  params.svg.innerHTML = "";
  // var points = generateCircularPoints(
  //   params.numberOfPoints,
  //   params.center,
  //   params.radius
  // );
  //connectEachPointWithExtendedLine(points, params.svg, 1000, params.strokeSize)
  var phiDots = generatePhiPoints(
    params.center,
    0.3,
    params.radius,
    1
  ).reverse();
  //[...Array(1000)].map((_,i) => (i*137.50776405) % 360)
  // drawCircles(phiDots, params.svg, params.radius, params.strokeSize);

  // drawPolygonFromPointsToCenter(
  //   3,
  //   phiDots,
  //   params.center,
  //   params.svg,
  //   params.strokeSize
  // );
  //  drawPointsToSvg(phiDots, params.svg, 1);

  const numberOfSpirals = params.sequnceNumber;
  connectSpiralsWithColor(
    numberOfSpirals,
    phiDots,
    params.svg,
    params.strokeSize
  );
  // connectEachPointWithExtendedLine(phiDots, params.svg, 1000, 0.5);
  // conndectPointsWithCenter(
  //   phiDots,
  //   params.center,
  //   params.svg,
  //   params.strokeSize
  // );
}

//   ${parseInt((((i % 3) + 2) / 3) * 255)},
