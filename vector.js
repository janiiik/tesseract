class Vector {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  static fromPolarRad(mag, rads) {
    return new Vector(mag * Math.cos(rads), mag * Math.sin(rads));
  }

  static fromPolarDeg(mag, degs) {
    return new Vector(
      mag * Math.cos(degToRad(degs)),
      mag * Math.sin(degToRad(degs))
    );
  }

  add(vec) {
    return new Vector(this.x + vec.x, this.y + vec.y);
  }

  sub(vec) {
    return new Vector(this.x - vec.x, this.y - vec.y);
  }

  mulScalar(val) {
    return new Vector(this.x * val, this.y * val);
  }

  divScalar(vec) {
    return new Vector(this.y / val, this.y / val);
  }

  get mag() {
    return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
  }

  set mag(val) {
    let ratio = val / this.mag;
    this.x = this.x * ratio;
    this.y = this.y * ratio;
  }

  get norm() {
    return new Vector(this.x / this.mag, this.y / this.mag);
  }

  get angleDeg() {
    return radToDeg(this.angleRad);
  }

  get angleRad() {
    return Math.atan2(this.y, this.x);
  }

  set angleDeg(degs) {
    let mag = this.mag;
    let rads = degToRad(degs);
    this.x = mag * Math.cos(rads);
    this.y = mag * Math.sin(rads);
  }

  set angleRad(rads) {
    let mag = this.mag;
    this.x = mag * Math.cos(rads);
    this.y = mag * Math.sin(rads);
  }

  getPoint() {
    return { x: this.x, y: this.y };
  }

  clone() {
    return new Vector(this.x, this.y);
  }
}

function radToDeg(rad) {
  return (180 * rad) / Math.PI;
}

function degToRad(deg) {
  return (deg * Math.PI) / 180;
}
